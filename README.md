# platformer

My attempt to create PC web based platfomer game with Phaser v2 in
HTML and Javascript.

Edit: Now in Saturn!

![1](https://user-images.githubusercontent.com/24475030/31047160-0887847e-a638-11e7-9c4b-71adacab2943.png)
![2](https://user-images.githubusercontent.com/24475030/31047162-08e643a6-a638-11e7-91d5-36e5af51d56d.png)
![3](https://user-images.githubusercontent.com/24475030/31047161-08e22eba-a638-11e7-9a83-f1177d94b09c.png)

## Getting Started

### Play

Visit demo (for device with keyboard including PC, Mac, etc) at http://faraco.github.io/platformer/index.html

### How to play?

Move with arrow keys.

Your goal is to collect 70 stars and reach score of 700 to win!

### TODO

* add timelapse
* add enemies and map resistances (void, lava fall, random gamma ray)
* add life system
* add highest score system (data keep in a server)
* add virtual joystick for mobile phone without physical keyboard access!

### Credits
* tileset by gfx0
* star image by Écrivain
* Theme music by ChaoticCycles
* sound effect by https://www.freesound.org/people/Mattix145

## Authors
* faraco <skelic3@gmail.com>

## License
This project is licensed under the GPL3 License - see the LICENSE file for details.
