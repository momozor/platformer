/**
 * @file app.js
 * @author faraco
 * @license MIT
 * 
 */

/**
 * @mainGame initialization.
 * 
 * Use Phaser.AUTO if the mobile support is done.
 */
let game = new Phaser.Game(800, 600, Phaser.CANVAS, 'game_canvas',
    {preload: preload, create: create, update: update});


/**
 *
 * Global variables.
 */
let map;
let layer1;

let score = 0;
let scoreText;
let timelapse = 0;
let timelapseText;

let player;
let platforms;
let stars;
let sfx;


/**
 * @desc Draw menu screen.
 * @return void
 */
function drawMenuScreen() {

}


/**
 *
 * @desc Draw collectable stars.
 *
 * @return void
 */
function drawStar() {
    for (let i = 0; i < 100; i++) {
            let star = stars.create(Math.random() * 1600, Math.random() * 1600, 'star');
            star.width = 50;
            star.height = 50;
            star.body.gravity.y = 200;
            star.body.bounce.y = 0.4 + Math.random() * 0.2;
        }
}


/**
 *
 * @desc Collect star logic (by main player).
 * 
 * @return void
 */
function collectStar(player, star) {
    sfx.play();
    score += 10;
    scoreText.text = 'Score: ' + score;
    star.kill();
}


/**
 * @ Restart the game.
 *
 * @return void
 */
function ifWinEvent() {
    if (score >= 700) {
        game.paused = true;
        let winText = game.add.text(game.height / 2, game.width / 2, 'You win!', { fontSize: '50px', fill: '#ffffff'});
        winText.fixedToCamera = true;

        let restartText = game.add.text(game.height / 2 - 70, game.width / 2 + 70, 'Click to restart', { fontSize: '50px', fill: '#00FF00'});
        restartText.inputEnabled = true;
        restartText.fixedToCamera = true;

        restartText.events.onInputDown.add(function() {
            game.paused = false;
            score = 0;
            game.state.restart();
        }, this);
    }
}

function preload() {
    game.load.image('bg1', 'assets/images/bg1.jpg');
    game.load.tilemap('map', 'assets/images/tile_csv/tilemap.csv');
    game.load.image('tiles', 'assets/images/tiles/landscape.png');
    game.load.spritesheet('player', 'assets/images/player/bat-sprite32x32.png', 32, 32, 16);
    game.load.image('star', 'assets/images/collectables/star.png');
    game.load.audio('mt1', 'assets/audio/saturn_sound.mp3');
    game.load.audio('pick', 'assets/audio/pick.wav');
}

function create() {

    // saturn background
    game.add.sprite(-300, 0, 'bg1');

    // theme music
    theme1 = game.add.audio('mt1');
    theme1.loopFull(0.6);

    sfx = game.add.audio('pick');

    map = game.add.tilemap('map', 32, 32);
    map.addTilesetImage('tiles');

    layer1 = map.createLayer(0);
    layer1.resizeWorld();

    // object (tile) ID collisions between others.
    map.setCollision(9);
    map.setCollision(34);
    map.setCollision(20);
    map.setCollision(25);
    map.setCollision(37);
    map.setCollision(32);
    map.setCollision(49);
    map.setCollision(51);
    map.setCollision(52);
    map.setCollision(53);

    stars = game.add.group();
    stars.enableBody = true;

    // draw 100 collectables stars at random position
    drawStar();

    scoreText = game.add.text(16, 16, 'score: 0', { fontSize: '32px', fill: '#ffffff'});
    scoreText.fixedToCamera = true;

    player = game.add.sprite(900, 50, 'player');
    game.physics.arcade.enable(player);

    player.body.bounce.y = 0.2;
    player.body.gravity.y = 300;
    player.body.collideWorldBounds = true;

    player.animations.add('still', [1, 2, 3], 10, true);
    player.animations.add('left', [13, 14, 15], 10, true);
    player.animations.add('right', [5, 6, 7], 10, true);

    game.camera.follow(player);
}

function update() {
    let hitPlatform = game.physics.arcade.collide(player, layer1);
    let starPlatform = game.physics.arcade.collide(stars, layer1);
    let starOverlap = game.physics.arcade.overlap(player, stars, collectStar);
    let cursors = game.input.keyboard.createCursorKeys();

    player.body.velocity.x = 0;
    if (cursors.left.isDown) {
            player.body.velocity.x = -150;
            player.animations.play('left');
        }

    else if (cursors.right.isDown) {
            player.body.velocity.x = 150;
            player.animations.play('right');
        }

    else {
        player.animations.play('still');
    }

    if (cursors.up.isDown && hitPlatform) {
        player.body.velocity.y = -350;
    }

    ifWinEvent();
}
