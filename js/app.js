/**
 * @file app.js
 * @author faraco
 * @license MIT
 *
 */
/**
 * @main Game initialization.
 *
 * @desc Use Phaser.AUTO if the mobile support is done.
 */
var game = new Phaser.Game(800, 600, Phaser.CANVAS, 'game_canvas', { preload: preload, create: create, update: update });
/**
 * @desc Global variables.
 */
var map;
var layer1;
;
var score = 0;
var scoreText;
var timelapse = 0;
var timelapseText;
var player;
var platforms;
;
var stars;
var sfx;
var theme1;
/**
 * @desc Draw menu screen.
 * @return void
 */
function drawMenuScreen() {
}
/**
 *
 * @desc Draw collectable stars.
 */
function drawStar() {
    for (var i = 0; i < 100; i++) {
        var star = stars.create(Math.random() * 1600, Math.random() * 1600, 'star');
        star.width = 30;
        star.height = 30;
        star.body.gravity.y = 200;
        star.body.bounce.y = 0.4 + Math.random() * 0.2;
    }
}
/**
 *
 *
 *
 */
function setMusicState() {
    if (theme1.isPlaying) {
        theme1.stop();
    }
    else {
        theme1.play();
    }
    console.log(theme1.isPlaying);
}
/**
 *
 * @desc Collect star logic (by main player).
 */
function collectStar(player, star) {
    sfx.play();
    score += 10;
    scoreText.text = 'Score: ' + score;
    star.kill();
}
/**
 * @desc Restart the game.
 *
 */
function ifWinEvent() {
    if (score >= 700) {
        game.paused = true;
        var winText = game.add.text(game.height / 2, game.width / 2, 'You win!', { fontSize: '50px', fill: '#ffffff' });
        winText.fixedToCamera = true;
        var restartText = game.add.text(game.height / 2 - 70, game.width / 2 + 70, 'Click to restart', { fontSize: '50px', fill: '#00FF00' });
        restartText.inputEnabled = true;
        restartText.fixedToCamera = true;
        restartText.events.onInputDown.add(function () {
            game.paused = false;
            score = 0;
            game.state.restart();
        }, this);
    }
}
/**
 * @desc Preload assets
 *
 */
function preload() {
    game.load.image('bg1', 'assets/images/bg1.jpg');
    game.load.tilemap('map', 'assets/images/tile_csv/tilemap.csv');
    game.load.image('tiles', 'assets/images/tiles/landscape.png');
    game.load.spritesheet('player', 'assets/images/player/bat-sprite32x32.png', 32, 32, 16);
    game.load.image('star', 'assets/images/collectables/star.png');
    game.load.audio('mt1', 'assets/audio/saturn_sound.mp3');
    game.load.audio('pick', 'assets/audio/pick.wav');
    // placeholders only (temporary)
    game.load.image('placeholder1', 'ph1');
}
/**
 * @desc Load & create object instances.
 *
 */
function create() {
    // saturn background
    game.add.sprite(-300, 0, 'bg1');
    // theme music
    theme1 = game.add.audio('mt1');
    theme1.loopFull(0.6);
    sfx = game.add.audio('pick');
    map = game.add.tilemap('map', 32, 32);
    map.addTilesetImage('tiles');
    layer1 = map.createLayer(0);
    layer1.resizeWorld();
    // object (tile) ID collisions between others.
    map.setCollision(9);
    map.setCollision(34);
    map.setCollision(20);
    map.setCollision(25);
    map.setCollision(37);
    map.setCollision(32);
    map.setCollision(49);
    map.setCollision(51);
    map.setCollision(52);
    map.setCollision(53);
    stars = game.add.group();
    stars.enableBody = true;
    drawStar();
    scoreText = game.add.text(16, 16, 'score: 0', { fontSize: '32px', fill: '#ffffff' });
    scoreText.fixedToCamera = true;
    player = game.add.sprite(game.world.centerX, game.world.centerY, 'player');
    game.physics.arcade.enable(player);
    player.body.bounce.y = 0.2;
    player.body.gravity.y = 300;
    player.body.collideWorldBounds = true;
    player.animations.add('still', [1, 2, 3], 10, true);
    player.animations.add('left', [13, 14, 15], 10, true);
    player.animations.add('right', [5, 6, 7], 10, true);
    game.camera.follow(player, Phaser.Camera.FOLLOW_LOCKON, 0.1, 0.1);
}
function moveLeft() {
    player.body.velocity.x = -150;
    player.animations.play('left');
}
function moveRight() {
    player.body.velocity.x = 150;
    player.animations.play('right');
}
function jump() {
    player.body.velocity.y = -350;
}
/**
 * @desc Update the game object instances based on defined logics.
 *
 *
 */
function update() {
    var hitPlatform = game.physics.arcade.collide(player, layer1);
    var starPlatform = game.physics.arcade.collide(stars, layer1);
    var starOverlap = game.physics.arcade.overlap(player, stars, collectStar);
    var cursors = game.input.keyboard.createCursorKeys();
    var keyboard = game.input.keyboard;
    //let w_key: Phaser.Game  = game.input.keyboard.addKey(Phaser.Keyboard.W);
    //let a_key: Phaser.Game = game.input.keyboard.addKey(Phaser.Keyboard.A);
    //let d_key: Phaser.Game = game.input.keyboard.addKey(Phaser.Keyboard.D);
    var m_key = game.input.keyboard.addKey(Phaser.Keyboard.M);
    player.body.velocity.x = 0;
    // cursors 
    if (cursors.left.isDown || keyboard.isDown(Phaser.Keyboard.A)) {
        moveLeft();
    }
    else if (cursors.right.isDown || keyboard.isDown(Phaser.Keyboard.D)) {
        moveRight();
    }
    else {
        player.animations.play('still');
    }
    if ((cursors.up.isDown || keyboard.isDown(Phaser.Keyboard.W)) && hitPlatform) {
        jump();
    }
    m_key.onDown.add(setMusicState, this);
    ifWinEvent();
}
//# sourceMappingURL=app.js.map
